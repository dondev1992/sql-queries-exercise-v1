-- 1 
INSERT INTO students (first_name, last_name, username, email)
VALUES ('Dan', 'Zanes', 'dzanes', 'dzanes@college.edu');
SELECT * FROM students;

-- 2 
UPDATE students
SET email = 'jblair21@college.edu'
WHERE id = 1;
SELECT * FROM students;

-- 3 
SELECT CONCAT(last_name, ', ', first_name) "Name", email "Email"
FROM students
ORDER BY last_name;

-- 4 
SELECT name, grade
FROM classes
INNER JOIN grades
ON grades.class_id = classes.id AND student_id = 3;

-- 5
DELETE FROM grades 
WHERE student_id = 3; 
DELETE FROM students 
WHERE id = 3;
SELECT * FROM students;

-- 6 
SELECT CONCAT(first_name, ' ', last_name) AS Fullname, grade 
FROM students 
INNER JOIN grades 
ON students.id = grades.student_id AND grades.class_id = 1;

-- STRETCH
SELECT DISTINCT CONCAT(first_name, ' ', last_name) AS Fullname name, grade AS Grade
FROM students
JOIN grades ON students.id = grades.student_id
JOIN classes ON classes.id = grades.class_id;